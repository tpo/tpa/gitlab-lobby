from django.db import models
from django import forms
from django.utils.translation import gettext_lazy as _

def is_valid_username(name):
    if not all(x.isalpha() or x=='_' or x.isnumeric() for x in name):
        raise forms.ValidationError(
            _('%(name)s is not a valid username. Should be A-Z, a-z, 0-9 and _.'),
            params={'name': name},
        )

def is_username_exists(name):
    if GitlabAccountRequest.objects.filter(username=name).count() > 0:
        raise forms.ValidationError(
            _('%(name)s already exists.'),
            params={'name': name},
        )


class GitlabAccountRequest(models.Model):
    username = models.CharField(max_length=64, validators=[is_valid_username,is_username_exists])
    email    = models.EmailField()
    reason   = models.CharField(max_length=256)
    approved = models.BooleanField(default=False)

class GitlabAccountRequestForm(forms.ModelForm):
    class Meta:
        model = GitlabAccountRequest
        fields = ["username", "email", "reason"]
